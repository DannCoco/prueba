<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login.in');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout');

/*
|--------------------------------------------------------------------------
| Seguridad rutas de la aplicación
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'auth'], function()
{
    Route::get('/', 'HomeController@index')->name('home');

    /**
     * 1. Personas
     */
    Route::resource('persons', 'Seguridad\PersonaController');
    
    /**
     * 2. Áreas
     */
    Route::resource('areas', 'Core\AreaController');

    /**
     * 3. Países
     */
    Route::resource('countries', 'Core\PaisController');

    /**
     * 4. Departamentos
     */
    Route::resource('states', 'Core\DepartamentoController');

    /**
     * 5. Ciudades
     */
    Route::resource('cities', 'Core\CiudadController');

    /**
     * 6. Email
     */
    Route::resource('email', 'Core\EmailController');
});
