@extends('admin.layouts.app')
@section('title', 'Email')
@section('components')
<div class="row d-flex justify-content-center">
    <div class="col-lg-8">
        <h5 class="one">Email</h5>
    </div>
</div>
@endsection
@section('content')
<div class="row d-flex justify-content-center">
    <div class="col-lg-12 col-xl-8">
        <div class="mail-body-content">
            {!! Form::open(['route' => 'email.store', 'id' => 'email-form']) !!}
                <div class="form-group">
                    <input type="text" name="email_to" class="form-control" placeholder="Para" required>
                </div>
                <div class="form-group">
                    <input type="text" name="subject" class="form-control" placeholder="Asunto">
                </div>
                <textarea name="description"></textarea>
                <br>
                <div class="row d-flex justify-content-center">
                    {!! Form::submit('Enviar', ['class' => 'btn btn-success btn-sm']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<!-- tinymce js -->
<script src="{{ asset('GradientAble/files/assets/pages/wysiwyg-editor/js/tinymce.min.js') }}"></script>
@endpush
@push('functions')
<script>
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea',
            height: 100,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true
        });
    });
</script>
@endpush