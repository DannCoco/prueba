<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'departamento';

	protected $primaryKey = 'codigo_departamento';
	   
	public static function states(Array $filters = [])
	{
		$query = self::query();
		$query->select('codigo_departamento as id', 'nombre_departamento as text');

		if(isset($filters['q']) && !empty($filters['q'])){
			$query->whereRaw("nombre_departamento like '%".$filters['q']."%'");
		}

		if(isset($filters['country']) && !empty($filters['country'])){
			$query->where('codigo_pais', $filters['country']);
		}

		if(isset($filters['id']) && !empty($filters['id'])){
			$query->where('codigo_departamento', $filters['id']);
		}

		$query->orderBy('nombre_departamento', 'asc');
		return $query->get();
	}
}
