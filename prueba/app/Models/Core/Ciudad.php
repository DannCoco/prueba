<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'ciudad';

	protected $primaryKey = 'codigo_ciudad';
	
	public static function cities(Array $filters = [])
	{
		$query = self::query();
		$query->select('codigo_ciudad as id', 'nombre_ciudad as text');

		if(isset($filters['q']) && !empty($filters['q'])){
			$query->whereRaw("nombre_ciudad like '%".$filters['q']."%'");
		}

		if(isset($filters['state']) && !empty($filters['state'])){
			$query->where('codigo_ciudad', $filters['state']);
		}

		if(isset($filters['id']) && !empty($filters['id'])){
			$query->where('codigo_ciudad', $filters['id']);
		}

		$query->orderBy('nombre_ciudad', 'asc');		
		return $query->get();
	}
}
