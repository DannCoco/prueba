<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

use App\Models\Seguridad\Persona;

class Area extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'areas';

	protected $primaryKey = 'codigo_area';
	   
	/**
	 * Get areas list
	 */
	public static function areas()
	{
		$query = Area::query();
		$query->select('codigo_area', 'nombre_area');
		return $query->get();
	}

	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function person()
	{
		return $this->belongsTo(Persona::class, 'codigo_area');
	}
}
