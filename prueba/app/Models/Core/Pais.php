<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'pais';

	protected $primaryKey = 'codigo_pais';
	   
	public static function countries(Array $filters = [])
	{
		$query = self::query();
		$query->select('codigo_pais as id', 'nombre_pais as text');

		if(isset($filters['q']) && !empty($filters['q'])){
			$query->whereRaw("nombre_pais like '%".$filters['q']."%'");
		}

		if(isset($filters['id']) && !empty($filters['id'])){
			$query->where('codigo_pais', $filters['id']);
		}
		
		$query->orderBy('nombre_pais', 'asc');
		return $query->get();
	}
}
