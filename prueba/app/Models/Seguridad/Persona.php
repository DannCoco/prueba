<?php

namespace App\Models\Seguridad;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Hash;

class Persona extends Authenticatable
{
    use Notifiable;

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'personas';

	protected $primaryKey = 'codigo_persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre_persona', 'cedula', 'fecha_nacimiento', 'sexo', 'codigo_pais', 'codigo_departamento', 'codigo_ciudad', 'email', 'username', 'codigo_area', 'rol', 'comentarios'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
	 * Gender
	 *
	 * @var array
	 */
    public static $gender = [1 => 'Hombre', 2  => 'Mujer', 3 => 'Prefiero no decirlo'];
    
    public static function getGender($gender)
    {
        if(in_array($gender, array_keys(self::$gender))) {
			return self::$gender[$gender];
		}
		return '';
    }

    /**
	 * Role
	 *
	 * @var array
	 */
	public static $roles = [1 => 'Admin', 2  => 'Equipo'];

    /**
     * Get user for login
     */
    public static function user(Array $filters = [])
    {
        $query = self::query();
        $query->select('codigo_persona', 'nombre_persona', 'cedula', 'fecha_nacimiento', 'sexo', 'codigo_pais', 'codigo_departamento', 'codigo_ciudad', 'email', 'nickname', 'codigo_area', 'password', 'rol', 'comentarios');

        if(isset($filters['username']) && !empty($filters['username'])){
            $query->whereRaw("nickname like '%".trim($filters['username'])."%'");
            $query->orWhereRaw("email like '%".trim($filters['username'])."%'");
        }

        if(isset($filters['password']) && !empty($filters['password'])){
            $query->where('password', Hash::make($filters['password']));
        }

        return $query->first();
    }

    /**
     * Get persons list
     */
    public static function persons()
    {
        $query = self::query();
        $query->select('codigo_persona', 'nombre_persona', 'cedula', 'fecha_nacimiento', 'sexo', 'username', 'rol');
        return $query->get();
    }
}
