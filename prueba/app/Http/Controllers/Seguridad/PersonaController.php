<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Log, DataTables;

use App\Models\Seguridad\Persona, App\Models\Core\Area;

class PersonaController extends Controller
{
    /**
     * Mostrar una lista del recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        if($request->ajax()){
            $gender = Persona::$gender;
            $persons = Persona::persons();
            
            return DataTables::of($persons)
                ->addColumn('sexo', function ($persons) use($gender){
                    return Persona::getGender($persons->sexo);
                })
                ->rawColumns(['sexo'])
                ->make(true);
        }
        return view('person.index');
    }

    /**
     * Muestra el formulario para crear un nuevo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $gender = Persona::$gender;
        $roles = Persona::$roles;
        $areas = Area::areas();
        return view('person.create-edit', compact('gender', 'roles', 'areas'));
    }

    /**
     * Almacenar un recurso recién creado en el almacenamiento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $data = $request->all();

            $person = new Persona;
            DB::beginTransaction();
            try {
                $person->fill($data);
                $person->password = bcrypt($request->password);
                $person->save();

                // Commit Transaction
                DB::commit();
                return response(['msg' => "Se ha creado la persona $person->nombre_persona correctamente", 'title' => '¡Éxito!'], 200)->header('Content-Type', 'application/json');

            }catch(\Exception $e){
                DB::rollback();
                Log::error(sprintf('%s:%s', 'PersonaController:store', $e->getMessage()));
                return response()->json(['success' => false, 'errors' => '¡Ha ocurrido un error!']);
            }
        }
        abort(404);
    }

    /**
     * Mostrar el recurso especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Muestra el formulario para editar el recurso especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Persona::find($id);
        if(!$person instanceof Persona){
            throw new \Exception("La persona no existe", $id);            
        }

        $gender = Persona::$gender;
        $roles = Persona::$roles;
        $areas = Area::areas();
        return view('person.create-edit', compact('person', 'gender', 'roles', 'areas'));
    }

    /**
     * Actualizar el recurso especificado en el almacenamiento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        if($request->ajax()){
            $data = $request->all();

            $person = Persona::find($id);
            if(!$person instanceof Persona){
                throw new \Exception("La persona no existe", $id);
            }

            DB::beginTransaction();
            try {
                $person->fill($data);
                $person->password = bcrypt($request->password);
                $person->save();

                // Commit Transaction
                DB::commit();
                return response(['msg' => "Se ha editado la persona $person->nombre_persona correctamente", 'title' => '¡Éxito!'], 200)->header('Content-Type', 'application/json');

            }catch(\Exception $e){
                DB::rollback();
                Log::error(sprintf('%s:%s', 'PersonaController:update', $e->getMessage()));
                return response()->json(['success' => false, 'errors' => '¡Ha ocurrido un error!']);
            }
        }
        abort(404);
    }

    /**
     * Eliminar el recurso especificado del almacenamiento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            throw new \Exception("No es numérico", $id);   
        }

        $person = Persona::find($id);
        if(!$person instanceof Persona){
            throw new \Exception("La persona no existe", $id);            
        }

        DB::beginTransaction();
        try 
        {
            $person->delete();

            // Commit Transaction
            DB::commit();
            return response(['msg' => 'Se ha eliminado la persona correctamente', 'title' => '¡Éxito!' ], 200)->header('Content-Type', 'application/json');
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::error(sprintf('%s:%s', 'PersonaController:destroy', $e->getMessage()));
            return response()->json(['success' => false, 'errors' => '¡Ha ocurrido un error!']);
        }
    }
}
