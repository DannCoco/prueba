<?php

namespace App\Http\Controllers\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Log, DataTables;

use App\Models\Core\Area;

class AreaController extends Controller
{
    /**
     * Mostrar una lista del recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        if($request->ajax()){
            $areas = Area::areas();
            return DataTables::of($areas)->make(true);
        }
        return view('area.index');
    }

    /**
     * Muestra el formulario para crear un nuevo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('area.create-edit');
    }

    /**
     * Almacenar un recurso recién creado en el almacenamiento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $area = new Area;
                $area->nombre_area = $request->nombre_area;
                $area->save();
    
                DB::commit();
                return response(['msg' => "Se ha creado el área $area->nombre_area correctamente", 'title' => '¡Éxito!'], 200)->header('Content-Type', 'application/json');
            }
            catch(\Exception $e)
            {
                DB::rollback();
                Log::error(sprintf('%s:%s', 'AreaController:store', $e->getMessage()));
                return response()->json(['success' => false, 'errors' => '¡Ha ocurrido un error!']);
            }
        }
        abort(404);
    }

    /**
     * Mostrar el recurso especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Muestra el formulario para editar el recurso especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!is_numeric($id)){
            throw new \Exception("El área no es número válido.", $id);   
        }

        $area = Area::find($id);
        if($area instanceof Area){
            return view('area.create-edit', compact('area'));            
        }
        abort(404);
    }

    /**
     * Actualizar el recurso especificado en el almacenamiento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $area = Area::find($id);
        if(!$area instanceof Area){
            throw new \Exception("No existe la área", $area);
        }
        
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $area->nombre_area = $request->nombre_area;
                $area->save();
    
                DB::commit();
                return response(['msg' => "Se ha editado el área $area->nombre_area correctamente", 'title' => '¡Éxito!'], 200)->header('Content-Type', 'application/json');
            }
            catch(\Exception $e)
            {
                DB::rollback();
                Log::error(sprintf('%s:%s', 'AreaController:update', $e->getMessage()));
                return response()->json(['success' => false, 'errors' => '¡Ha ocurrido un error!']);
            }
        }
        abort(404);
    }

    /**
     * Eliminar el recurso especificado del almacenamiento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            throw new \Exception("No es numérico", $id);   
        }

        $area = Area::find($id);
        if(!$area instanceof Area){
            throw new \Exception("El área no existe", $id);            
        }

        if(!empty($area->person)){
            return response([
                'errors' => ['El área se encuentra asignado a una o más personas, verifique.'],
                'title' => '¡Error!'
            ], 422)->header('Content-Type', 'application/json');
        }

        DB::beginTransaction();
        try 
        {
            $area->delete();

            // Commit Transaction
            DB::commit();
            return response(['msg' => 'Se ha eliminado el área correctamente', 'title' => '¡Éxito!' ], 200)->header('Content-Type', 'application/json');
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::error(sprintf('%s:%s', 'AreaController:destroy', $e->getMessage()));
            return response()->json(['success' => false, 'errors' => '¡Ha ocurrido un error!']);
        }
    }
}
