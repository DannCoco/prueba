<?php

namespace App\Http\Controllers\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Log, Auth;

use App\Jobs\SendEmail;

class EmailController extends Controller
{
    /**
     * Mostrar una lista del recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        return view('email.send-email');
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		try {
			$data = $request->all();
			$data['user_send'] = Auth::user()->nombre_usuario;
			$data['user_send_mail'] = Auth::user()->email;

			dispatch(new SendEmail($data));
			
			return redirect()->route('email.index')->with('message', "Se ha enviado el correo a $request->email_to");
		} catch (\Exeption $e) {
			Log::error(sprintf('%s:%s', 'EmailController:store', $e->getMessage()));
			return redirect()->route('email.index')->with('errors', "No se envió el correo a $request->email_to");
		}
    }
}
