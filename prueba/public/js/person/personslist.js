$(document).ready(function() {
    table = $('#table-person').DataTable({
        serverSide: false,
        stateSave: true,
        keys: true,
        scrollY: "420px",
        scrollX: "auto",
        scrollCollapse: true,
        paging:true,
        dom: 'Bfrtip',
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ Registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "Filtrado de un total de _MAX_ registros",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        buttons: [
            {
                className: 'btn-sm',
                text: '<i class="icofont icofont-plus" data-toggle="tooltip" title="Crear persona"></i>',
                action: function(e){
                    e.preventDefault();
                    window.location.href = document.URL+'/create';                
                }
            },
            {
                className: 'btn-sm',
                text: '<i class="fa fa-md fa-file-excel-o" data-toggle="tooltip" title="Exportar excel"></i>',
                extend: 'excel',
                extension: '.xls'
            }, 
            {
                className: 'btn-sm',
                text: '<i class="fa fa-md fa-file-pdf-o" data-toggle="tooltip" title="Exportar PDF"></i>',
                extend: 'pdf',
                extension: '.pdf'
            },
            {
                className: 'btn-sm',
                text: '<i class="fa fa-md fa-print" data-toggle="tooltip" title="Imprimir"></i>',
                extend: 'print',
                extension: '.print'
            }
        ],
        ajax: document.URL,
        columns: [
            {data: 'codigo_persona', name: "codigo_persona", className: "all"},
            {data: 'nombre_persona', name: "nombre_persona", className: "all"},
            {data: 'cedula', name: "cedula", className: "all"},
            {data: 'fecha_nacimiento', name: "fecha_nacimiento", className: "all"},
            {data: 'sexo', name: "sexo", className: "all"}
        ],
        columnDefs: [
            {
                targets: 5,
                width: '10%',
                orderable: false,
                className: 'text-center',
                render: function ( data, type, full, row ) {
                    if(full.rol != 1 && full.nickname != "admin"){
                        return '<div class="ropdown-primary dropdown open">'+
                            '<button type="button" class="btn btn-default dropdown-toggle" title="Opciones" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                '<i class="fa fa-ellipsis-v"></i>'+
                            '</button>'+
                            '<div class="dropdown-menu" x-placement="bottom-start" aria-labelledby="dropdown-2" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">'+
                                '<a class="dropdown-item waves-light waves-effect" href="'+ document.URL+'/'+ full.codigo_persona +'/edit">'+
                                    '<i class="fa fa-pencil"></i> Editar'+
                                '</a>'+    
                                '<div class="dropdown-divider"></div>'+
                                '<a class="dropdown-item waves-light waves-effect delete" data-id="'+full.codigo_persona+'" href="#">'+
                                    '<i class="fa fa-trash"></i> Eliminar'+
                                '</a>'+
                            '</div>'+
                        '</div>';
                    }else{
                        return '<div class="ropdown-primary dropdown open">'+
                            '<button type="button" class="btn btn-default dropdown-toggle" title="Opciones" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                '<i class="fa fa-ellipsis-v"></i>'+
                            '</button>'+
                            '<div class="dropdown-menu" x-placement="bottom-start" aria-labelledby="dropdown-2" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">'+
                                '<a class="dropdown-item waves-light waves-effect">'+
                                    '<i class="fa fa-ban"></i> No hay acciones'+
                                '</a>'+
                            '</div>'+
                        '</div>';
                    }
                }
            }
        ]
    });

    table.on('click', '.delete', function (e) {
        e.preventDefault();
        
        var codeArea = $(e.target).attr('data-id');
        
        SwalDelete(codeArea);
    });
});

function SwalDelete(id) {
    swal({
        title: "Confirmación",
        text: "¿Está seguro de eliminar este registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        cancelButtonColor: '#d33',
        showLoaderOnConfirm: true,
        closeOnConfirm: false
    }, function (isConfirm) {
        
        if (!isConfirm) return;
        $.ajax({
            type: 'DELETE',
            url: document.URL +'/'+ id,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
            },
            success: function (response, NULL, jqXHR) {
                table.ajax.reload();
            },
        })
        .done(function (response) {
            swal("¡Registro eliminado exitosamente!", response.message, response.status);
        })
        .fail(function (response) {
            var msg = '',
                errores = response.responseJSON.errors;

            $.each(errores, function (name, val) {
                msg += val;
            });
            swal('¡Error!', msg, 'error');
        });
    });
}