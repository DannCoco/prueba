$(document).ready(function () {

    var that = this,
        form = $("#person-form");

    this.pais = $("#codigo_pais");
    this.departamento = $("#codigo_departamento");
    this.ciudad = $("#codigo_ciudad");

    select2Trigger();

    $(form).parsley({
        trigger: 'change',
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function (el) {
            return el.$element.closest('.form-group');
        },
        errorsWrapper: '<p style="color:red;" class="help-block help-block-error"></p>',
        errorTemplate: '<span></span>',
    });

    form.submit(function(e) {
        e.preventDefault();
        var type = "POST",
            target = $(e.target);
        
        if (target.find($("#person-success")).attr("data-resource") == "edit"){
            type = "PUT";
        }

        $.ajax({
            url: form.attr('action'),
            type: type,
            data: target.serialize(),
            success: function (response, NULL, jqXHR) {
                if (target.find($("#person-success")).attr("data-resource") == "create"){
                    form[0].reset();  // Reset all form data
                }
                
                new PNotify({
                    title: '¡Éxito!',
                    text: response.msg,
                    type: 'success',
                });
            },
            error: function (data) {
                var msg = '',
                    errores = data.responseJSON.errors;

                $.each(errores, function (name, val) {
                    msg += val + '<br>';
                });
                new PNotify({
                    title: "¡Error!",
                    text: msg,
                    type: 'error',
                });
            }
        });
    });

    /**
    * init select2 plugin
    */
    function select2Trigger() {
console.log('entra');

        var _this = this,
            config = {
              '.choice-select' : {language:"es", placeholder:'Seleccione una opción'},
              '.choice-select-deselect'  : {language:"es", placeholder:'Seleccione una opción', allowClear: true},
              '.choice-select-no-single' : {},
              '.choice-select-multiple': {},
              '.choice-select-autocomplete': {
                language: "es",
                allowClear: true,
                placeholder:'Seleccione una opción',
                ajax: {
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page,
                            denied:  $(this).data('denied'),
                            lang:  $(this).data('lang'),
                            data: $(this).data('param'),
                            city: $($(this).data('val')).find(":selected").val(),
                            location: $('#codigo_localidad_ciudad').find(":selected").val()
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    escapeMarkup: function (markup) { return markup; },
                    cache: true,
                    minimumInputLength: 1
                }
              }
            };

        // Instance selects to choice plugin
        for (var selector in config){

            $(selector).each(function(index, el) {
                var $el = $(el);

                if( $el.data('select2') == undefined ){
                    $el.select2(config[selector]);

                    $el.on('select2:open', function (evt) {
                        var elSelect = $(evt.target);
                        var w = elSelect.next('.select2-container').css('width');

                        $('.select2-dropdown').css('min-width', w);
                    });

                    // set default option
                    if(selector == '.choice-select-autocomplete') {

                        var initialId = $el.data('initial-value');
                        var $option = null;

                        if(initialId) {
                            var ajaxOptions = $el.data('select2').dataAdapter.ajaxOptions;

                            $option = $('<option selected>Loading...</option>').val(initialId);
                            $el.append($option).trigger('change');

                            $.get( ajaxOptions.url, {id:initialId}, function(data) {
                                $option.text(data[0].text).val(data[0].id);
                                $option.removeData();
                                $el.trigger('change');
                            });
                        }

                        $el.off('select2:unselect')
                        $el.on('select2:unselect', function (event) {
                            var $option = $('<option value="" selected></option>');
                            var $selected = $(event.target);

                            $selected.find('option:selected')
                            .remove()
                            .end()
                            .append($option)
                            .trigger('change');

                        });
                    }
                }
            });
        }
    };
});