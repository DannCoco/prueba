$(document).ready(function() {
    $('#login-form').parsley({
        trigger: 'change',
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function (el) {
            return el.$element.closest('.form-group');
        },
        errorsWrapper: '<p style="color:red;" class="help-block help-block-error text-left"></p>',
        errorTemplate: '<span></span>',
    });
});