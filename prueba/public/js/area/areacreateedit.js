$(document).ready(function () {

    this.$nombreAea = $('#nombre_area');
    var that = this,
        form = $("#area-form");

    $(form).parsley({
        trigger: 'change',
        successClass: "has-success",
        errorClass: "has-error",
        classHandler: function (el) {
            return el.$element.closest('.form-group');
        },
        errorsWrapper: '<p style="color:red;" class="help-block help-block-error"></p>',
        errorTemplate: '<span></span>',
    });

    form.submit(function(e) {
        e.preventDefault();
        var type = "POST",
            target = $(e.target);
        
        if (target.find($("#area-success")).attr("data-resource") == "edit"){
            type = "PUT";
        }

        $.ajax({
            url: form.attr('action'),
            type: type,
            data: target.serialize(),
            success: function (response, NULL, jqXHR) {
                if (target.find($("#area-success")).attr("data-resource") == "create"){
                    that.$nombreAea.val('');
                }
                
                new PNotify({
                    title: '¡Éxito!',
                    text: response.msg,
                    type: 'success',
                });
            },
            error: function (data) {
                var msg = '',
                    errores = data.responseJSON.errors;

                $.each(errores, function (name, val) {
                    msg += val + '<br>';
                });
                new PNotify({
                    title: "¡Error!",
                    text: msg,
                    type: 'error',
                });
            }
        });
    });
});