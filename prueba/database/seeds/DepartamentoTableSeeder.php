<?php

use Illuminate\Database\Seeder;

use App\Models\Core\Departamento;

class DepartamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Departamento::insert([
            ['nombre_departamento' => 'Amazonas', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Antioquia', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Arauca', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Atlántico', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Bolivar', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Boyacá', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Caldas', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Caquetá', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Casanare', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Cauca', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Cesar', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Chocó', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Córdoba', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Cundinamarca', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Guainía', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Guaviare', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Huila', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'La Guajira', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Magdalena', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Meta', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Nariño', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Norte de Santander', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Putumayo', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Quindio', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Risaralda', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'San Andres y Providencia', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Santander', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Sucre', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Tolima', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Valle del Cauca', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Vaupés', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['nombre_departamento' => 'Vichada', 'codigo_pais' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
        ]);
    }
}
