<?php

use Illuminate\Database\Seeder;

use App\Models\Seguridad\Persona;

class PersonasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Persona::insert([
            [
                'nombre_persona' => 'Administrador', 
                'cedula' => '123456789',
                'fecha_nacimiento' => '1987-11-06',
                'sexo' => '1',
                'codigo_pais' => 1,
                'codigo_departamento' => 1,
                'codigo_ciudad' => 1,
                'email' => 'admin@correo.com',
                'username' => 'admin',
                'codigo_area' => 1,
                'password' => bcrypt('123456'),
                'rol' => 1,
                'comentarios' => 'Administrador del sistema',
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')
            ]        
        ]);
    }
}
