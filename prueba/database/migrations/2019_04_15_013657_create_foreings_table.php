<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('departamento', function (Blueprint $table) {
            $table->foreign('codigo_pais')->references('codigo_pais')->on('pais')
                ->onDelete('cascade');
        });

        Schema::connection('mysql')->table('ciudad', function (Blueprint $table) {
            $table->foreign('codigo_departamento')->references('codigo_departamento')->on('departamento')
                ->onDelete('cascade');
        });

        Schema::connection('mysql')->table('personas', function (Blueprint $table) {
            $table->foreign('codigo_pais')->references('codigo_pais')->on('pais')
                ->onDelete('cascade');
        });

        Schema::connection('mysql')->table('personas', function (Blueprint $table) {
            $table->foreign('codigo_departamento')->references('codigo_departamento')->on('departamento')
                ->onDelete('cascade');
        });

        Schema::connection('mysql')->table('personas', function (Blueprint $table) {
            $table->foreign('codigo_ciudad')->references('codigo_ciudad')->on('ciudad')
                ->onDelete('cascade');
        });

        Schema::connection('mysql')->table('personas', function (Blueprint $table) {
            $table->foreign('codigo_area')->references('codigo_area')->on('areas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreings');
    }
}
